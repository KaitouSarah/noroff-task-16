# Noroff Task 16

Task 16: IEnumerable upgrade
Reverse engineer the code on: https://docs.microsoft.com/enus/dotnet/api/system.collections.ienumerable?view=net framework-4.8 

Use it to create an enumerable collection for any of you previous objects created in previous tasks 

Demonstrate that it works by running a sample iteration on the collection using a foreach

Create a separate IEnumerable collection to run a LINQ query on.

Note 1: I used the Person object from task 5.

Note 2: Was a bit unsure of the "Create a separate IEnumerable..." part. Just created a new instance of the contacts-class, hope that is ok.

Note 3: Think I messed up in one commit. Reset did not work even though I had not pushed? Weird. 