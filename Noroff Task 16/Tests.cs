﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Noroff_Task_16
{
    class Tests
    {
        //Sample iteration on the IEnumerable collection using a foreach
        public void testIenumerableIteration()
        {
            Person[] contactArray = new Person[3]
            {
                new Person("David", "Draiman", "91929394", "david@draiman.com"),
                new Person("Ivan", "Moody", "92939495", "ivan@moody.com"),
                new Person("Matthew", "Shadows", "93949596", "matthew@shadows.com")
            };

            Contacts contactList = new Contacts(contactArray);

            Console.WriteLine("Results from IEnumerable iteration: ");

            foreach (Person p in contactList)
            {
                Console.WriteLine(p.FirstName + " " + p.LastName);
            }
        }

        //Sample LINQ query on other IEnumerable collection
        public void testLinqQuery()
        {
            Person[] contactArray = new Person[3]
            {
                new Person("James", "Hetfield", "94959697", "james@hetfield.com"),
                new Person("Lars", "Ulrich", "95969798", "lars@ulrich.com"),
                new Person("Kirk", "Hammett", "96979899", "kirk@hammett.com")
            };
            Contacts contactList = new Contacts(contactArray);

            var resultLinq = //Could also be IEnumerable<Person>
                from person in contactList.ContactList
                where person.GetFullName().Contains('l')
                select person;

            Console.WriteLine("~~~~~~~~~~~~~~\nResults from LINQ query: ");

            foreach (Person p in resultLinq)
            {
                Console.WriteLine($"{p.FirstName} {p.LastName}");
            }

        }
    }
}
