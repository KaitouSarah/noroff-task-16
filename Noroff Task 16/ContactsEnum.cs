﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_16
{
    class ContactsEnum : IEnumerator
    {
        public Person[] _contacts;

        // Enumerators are positioned before the first element
        // until the first MoveNext() call.
        int position = -1;

        public ContactsEnum(Person[] list)
        {
            _contacts = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < _contacts.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Person Current
        {
            get
            {
                try
                {
                    return _contacts[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
