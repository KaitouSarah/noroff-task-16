﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_16
{
    class Person
    {
        string firstName;
        string lastName;
        string telephone;
        string email;

        public Person(string fn, string ln, string tl, string em)
        {
            firstName = fn;
            lastName = ln;
            telephone = tl;
            email = em;
        }

        public void PrintContactInfo()
        {
            Console.WriteLine($"{lastName}, {firstName} \n{telephone}\n{email}\n");
        }

        /*Method for getting the full name in lower case. 
        Better for searching.*/
        public string GetFullName()
        {
            return (FirstName + " " + lastName).ToLower();
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
    }

}
