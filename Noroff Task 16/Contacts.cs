﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_16
{
    class Contacts : IEnumerable
    {
        private Person[] contactList;


        public Contacts(Person[] contactArray)
        {
            contactList = new Person[contactArray.Length];

            for (int i = 0; i < contactArray.Length; i++)
            {
                contactList[i] = contactArray[i];
            }
        }

        // Implementation for the GetEnumerator method.
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public ContactsEnum GetEnumerator()
        {
            return new ContactsEnum(contactList);
        }

        internal Person[] ContactList { get => contactList; set => contactList = value; }
    }
}
