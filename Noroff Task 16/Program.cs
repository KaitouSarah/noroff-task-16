﻿using System;


namespace Noroff_Task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Tests tests = new Tests();

            tests.testIenumerableIteration();
            tests.testLinqQuery();
        }

    }
}

